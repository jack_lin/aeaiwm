<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<title></title>
<style>
body{
	font-family: SimSun;
}
h2{
	mso-style-link:"标题 2 Char";
	margin-top:13.0pt;
	margin-right:0cm;
	margin-bottom:13.0pt;
	margin-left:0cm;
	text-align:justify;
	text-justify:inter-ideograph;
	line-height:173%;
	page-break-after:avoid;
	font-size:16.0pt;
	font-family:"Cambria","serif";
}
p.MsoNormal, li.MsoNormal, div.MsoNormal{
	margin:0cm;
	margin-bottom:.0001pt;
	text-align:justify;
	text-justify:inter-ideograph;
	font-size:10.5pt;
	font-family:"Times New Roman","serif";
}

WordSection1
	{size:595.3pt 841.9pt;
	margin:72.0pt 90.0pt 72.0pt 90.0pt;
	layout-grid:15.6pt;}
div.WordSection1
	{page:WordSection1;}

</style>
</head>
<body bgcolor=white lang=ZH-CN style='text-justify-trim:punctuation'>

<div class="WordSection1" style='layout-grid:15.6pt;table-layout:fixed;word-break:break-strict;'>
<#list model as log>
	<h2>${log.date}</h2>   
		<div id="li">
			<#list log.contentList as content>
				<p class="MsoNormal" align=left style='margin-left:18.0pt;text-align:left;text-indent:-18.0pt;background:white'>
				${content.text}</p>
			</#list>
		</div>
 </#list>
</div>
</body>
</html>
