package com.agileai.wm.module.weektime.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.StandardListHandler;
import com.agileai.wm.cxmodule.WmWeektimeManage;

public class WmWeektimeManageListHandler
        extends StandardListHandler {
    public WmWeektimeManageListHandler() {
        super();
        this.editHandlerClazz = WmWeektimeManageEditHandler.class;
        this.serviceId = buildServiceId(WmWeektimeManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
    }

    protected WmWeektimeManage getService() {
        return (WmWeektimeManage) this.lookupService(this.getServiceId());
    }
}
