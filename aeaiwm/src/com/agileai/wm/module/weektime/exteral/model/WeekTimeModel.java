package com.agileai.wm.module.weektime.exteral.model;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace="http://www.agileai.com/wm/service")
public class WeekTimeModel implements Serializable{
	private static final long serialVersionUID = -195179097139582084L;
	
	private String id = null;
	private String stime = null;
	private String etime = null;
	private String standDay = null;
	private String type = null;
	
	public WeekTimeModel(){
	}

	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getStime() {
		return stime;
	}

	public void setStime(String stime) {
		this.stime = stime;
	}

	public String getEtime() {
		return etime;
	}

	public void setEtime(String etime) {
		this.etime = etime;
	}

	public String getStandDay() {
		return standDay;
	}

	public void setStandDay(String standDay) {
		this.standDay = standDay;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}
