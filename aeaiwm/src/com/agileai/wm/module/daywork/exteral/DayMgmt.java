package com.agileai.wm.module.daywork.exteral;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/rest")
public interface DayMgmt {
    
    @GET
    @Path("/get-daywork-info")  
    @Produces(MediaType.APPLICATION_JSON)
	public String getDayWorkInfo(); 
    
    @GET
    @Path("/find-daywork-infos")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findDayWorkInfos(); 
    
}
