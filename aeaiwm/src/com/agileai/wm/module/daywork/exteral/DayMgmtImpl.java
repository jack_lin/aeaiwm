package com.agileai.wm.module.daywork.exteral;

import java.util.Date;
import java.util.List;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;

import com.agileai.domain.DataRow;
import com.agileai.hotweb.controller.core.BaseHandler;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.ws.BaseRestService;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmDayworkManage;

public class DayMgmtImpl extends BaseRestService implements DayMgmt {

	@Override
	public String getDayWorkInfo() {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
			String currentTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			
			JSONObject jsonObject = new JSONObject();
			DataRow curDayWorkRow = wmDayworkManage.findCurrentDayworkRecords(userId,currentTime);
			JSONArray jsonArray = new JSONArray();
			JSONObject jsonObject2 = new JSONObject();
			if(curDayWorkRow != null && curDayWorkRow.size() > 0){
				jsonObject2.put("id", curDayWorkRow.stringValue("TW_ID"));
				jsonObject2.put("content", curDayWorkRow.stringValue("TW_CONTENT"));
			}else{
				jsonObject2.put("content", "无记录");
			}
			jsonArray.put(jsonObject2);
			
			jsonObject.put("today", jsonArray);
			responseText = jsonObject.toString();
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}

	@Override
	public String findDayWorkInfos() {
		String responseText = BaseHandler.FAIL;
		try {
			User user = (User)this.getUser();
			String userId = user.getUserId();
//			String minTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,DateUtil.getDateAdd(new Date(), DateUtil.DAY, -1));
			String currentTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date());
			String maxTime = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL, DateUtil.getDateAdd(new Date(), DateUtil.DAY, 1));
			WmDayworkManage wmDayworkManage = this.lookupService(WmDayworkManage.class);
			
			List<DataRow> pastDayworkRecords = wmDayworkManage.findPastDayWorkRecords(userId,currentTime);
			JSONObject jsonObject = new JSONObject();
			JSONArray pastDayArray = new JSONArray();
			if(pastDayworkRecords.size() != 0){
				for(int i=0;i<pastDayworkRecords.size();i++){
					DataRow row = pastDayworkRecords.get(i);
					JSONObject pastDayJson = new JSONObject();
					String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
					String time = row.stringValue("TW_TIME");
					time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,row.getTimestamp("TW_TIME"));
					time = time.substring(5, 10);
					pastDayJson.put("time", time);
					pastDayJson.put("weekText", weekText);
					pastDayJson.put("id", row.stringValue("TW_ID"));
					pastDayJson.put("content", row.stringValue("TW_CONTENT"));
					pastDayJson.put("env", row.stringValue("TW_ENV_NAME"));
					pastDayArray.put(pastDayJson);
				}
			}else{
				JSONObject pastDayJson = new JSONObject();
				pastDayJson.put("content", "无记录");
				pastDayArray.put(pastDayJson);
			}
			
//			DataRow currentDayworkRecord = wmDayworkManage.findCurrentDayworkRecords(userId,currentTime);
//			JSONArray curDayArray = new JSONArray();
//			if(currentDayworkRecord != null){
//				JSONObject curDayJson = new JSONObject();
//				String weekText = DateUtil.getWeekText(currentDayworkRecord.getTimestamp("TW_TIME"));
//				String time = currentDayworkRecord.stringValue("TW_TIME");
//				time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,currentDayworkRecord.getTimestamp("TW_TIME"));
//				time = time.substring(5, 10);
//				curDayJson.put("time", time);
//				curDayJson.put("weekText", weekText);
//				curDayJson.put("id", currentDayworkRecord.stringValue("TW_ID"));
//				curDayJson.put("content", currentDayworkRecord.stringValue("TW_CONTENT"));
//				curDayJson.put("env", currentDayworkRecord.stringValue("TW_ENV_NAME"));
//				curDayArray.put(curDayJson);
//			}else{
//				JSONObject curDayJson = new JSONObject();
//				curDayJson.put("content", "无记录");
//				curDayArray.put(curDayJson);
//			}
			
			List<DataRow> fllowDayworkRecords = wmDayworkManage.findFllowDayworkRecords(userId,maxTime);
			JSONArray fllowDayArray = new JSONArray();
			for(int i=0;i<fllowDayworkRecords.size();i++){
				DataRow row = fllowDayworkRecords.get(i);
				JSONObject fllowDayJson = new JSONObject();
				String weekText = DateUtil.getWeekText(row.getTimestamp("TW_TIME"));
				String time = row.stringValue("TW_TIME");
				time = DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,row.getTimestamp("TW_TIME"));
				time = time.substring(5, 10);
				fllowDayJson.put("time", time);
				fllowDayJson.put("weekText", weekText);
				fllowDayJson.put("id", row.stringValue("TW_ID"));
				fllowDayJson.put("content", row.stringValue("TW_CONTENT"));
				fllowDayJson.put("env", row.stringValue("TW_ENV_NAME"));
				fllowDayArray.put(fllowDayJson);
			}
			
			List<DataRow> noteRecords = wmDayworkManage.initNoteRecords(userId);
			JSONArray noteArray = new JSONArray();
			if(noteRecords.size() != 0){
				int size = 5;
				if(noteRecords.size() < 5){
					size = noteRecords.size();
				}
				for(int i=0;i<size;i++){
					DataRow row = noteRecords.get(i);
					JSONObject notejson = new JSONObject();
					notejson.put("title", row.stringValue("NOTE_TITLE"));
					notejson.put("content", row.stringValue("NOTE_DESCRIBE"));
					notejson.put("id", row.stringValue("NOTE_ID"));
					noteArray.put(notejson);
				}
			}else{
				JSONObject notejson = new JSONObject();
				notejson.put("content", "无记录");
				noteArray.put(notejson);
			}
			
			jsonObject.put("past", pastDayArray);
//			jsonObject.put("currentday", curDayArray);
			jsonObject.put("follow", fllowDayArray);
			jsonObject.put("notes", noteArray);
			responseText = jsonObject.toString();
			
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(),e);
		}
		return responseText;
	}


}
