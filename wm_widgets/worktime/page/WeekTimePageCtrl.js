angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.findWeekTimeListInfos = function(){
		var url = '/aeaiwm/services/WeekTime/rest/find-weektimelist-infos';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.weekTime = rspJson.weekTime;
		});
	}
	
	$scope.resetDailyWork = function(){
		$scope.weektime = {"id":"","startTime":"","endTime":"","standDay":"","type":""}
	}
	
	$scope.doCreateModal = function(){
		$scope.resetDailyWork();
		AppKit.createModal("${menuCode}","WeekTimeCreateModal",$scope);
	}
	
	$scope.openModal = function(weekTimeId,text) {
		$scope.currentId = weekTimeId;
		$scope.showSave = true;
		if('showDetail'== text){
			$scope.showSave = false;
		}
		AppKit.createModal("${menuCode}","WeekTimeEditModal",$scope);

		var url = '/aeaiwm/services/WeekTime/rest/get-week-time/'+weekTimeId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.weektime = rspJson;
			$scope.weektime.stime = new Date($scope.weektime.stime);
			$scope.weektime.etime = new Date($scope.weektime.etime);
		});
	}
	
	$scope.delWeekTime = function(weekTimeId) {
		var url = '/aeaiwm/services/WeekTime/rest/del-week-time/'+weekTimeId;
		AppKit.getJsonApi(url).then(function(response){
			if ("success" == response.data){
				$scope.findWeekTimeListInfos();
				AppKit.successPopup();	
			}else if("fail" == response.data){
				AppKit.errorPopup();
			}
		});
	}
	
	$scope.updateWorkTimeInfo = function(){
		$scope.weektime.id = $scope.currentId;
		$scope.weektime.type = 'update';
		var url = "/aeaiwm/services/WeekTime/rest/save-week-time"
		AppKit.postJsonApi(url,$scope.weektime).then(function(response){
			if ("success" == response.data){
				$scope.findWeekTimeListInfos();
				AppKit.successPopup();	
				AppKit.closeModal();
			}else if("fail" == response.data){
				AppKit.errorPopup();
			}else if("error" == response.data){
				AppKit.errorPopup({"title":"周期定义不能超过10天!"});
			}else{
				AppKit.errorPopup({"title":"结束时间需大于起始时间！"});
			}
		});
	}
	
	$scope.calculateStandDay = function(stime,etime) {
		$scope.stime = stime;
		$scope.etime = etime;
		var url = '/aeaiwm/services/WeekTime/rest/calculate-stand-day/'+stime+'/'+etime;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.weektime = rspJson;
			$scope.weektime.stime = new Date($scope.weektime.stime);
			$scope.weektime.etime = new Date($scope.weektime.etime);
		});
	}
	
	$scope.weektime = {"id":"","startTime":"","endTime":"","standDay":"","type":""}
	$scope.creteWorkTimeListInfo = function(){
		$scope.weektime.type = 'create';
		var url = "/aeaiwm/services/WeekTime/rest/save-week-time"
		AppKit.postJsonApi(url,$scope.weektime).then(function(response){
			if ("success" == response.data){
				$scope.findWeekTimeListInfos();
				AppKit.successPopup();	
				AppKit.closeModal();
			}else if("fail" == response.data){
				AppKit.errorPopup();
			}else if("error" == response.data){
				AppKit.errorPopup({"title":"周期定义不能超过10天!"});
			}else{
				AppKit.errorPopup({"title":"结束时间需大于起始时间！"});
			}
		});
	}
});