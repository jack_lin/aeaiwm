angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.limitSize = 5
	$scope.showionic = null;
	$scope.startTime = null;
	$scope.endTime = null;
	$scope.data = {grpId:''};
	$scope.isExistLeftHide = function(){
		if ($scope.curentIndex > $scope.limitSize/2){
			return true;
		}else{
			return false;
		}	
	};
	
	$scope.isExistRightHide = function(){
		if (($scope.totalSize - $scope.curentIndex) < ($scope.limitSize/2 + 1)){
			return false;
		}else{
			return true;
		}				
	};
	
	$scope.setActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.showIonic= function(activeTab){
		if(activeTab == $scope.activeDayTab){
			return true;
		}else{
			return false;
		}
	};
	
	$scope.showPolicy = function(itemIndex){
		var rangeDiff = $scope.limitSize/2 + 1;
		var curentIndex = $scope.curentIndex;
		if($scope.totalSize <= $scope.limitSize){
			//不显示箭头图片
			$scope.showLeftionic = false;
			$scope.showRightionic = false;
			return true;
		}else{
			if (curentIndex >=rangeDiff){
				//显示向左箭头
				$scope.showLeftionic = true;
				$scope.showRightionic = false;
				
				if($scope.totalSize - curentIndex <= $scope.limitSize/2){
					if(itemIndex >= $scope.totalSize - $scope.limitSize){
						return true;
					}else{
						return false;
					}
				}else{
					//显示向左、右的箭头
					$scope.showLeftionic = true;
					$scope.showRightionic = true;
					if (itemIndex <= curentIndex + $scope.limitSize/2 && itemIndex > curentIndex - $scope.limitSize/2){
						return true;
					}else{
						return false;
					}
				}
			}else{
				//显示向右箭头
				$scope.showLeftionic = false;
				$scope.showRightionic = true;
				
				if(itemIndex <$scope.limitSize){
					return true;
				}else{
					return false;
				}
			}
		}
		
	}
	
	$scope.onSwipeLeft = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == ($scope.tabs.length-1))continue;
				$scope.activeTab = $scope.tabs[i+1];
				$scope.changeClass($scope.activeTab);
				$scope.findActiveUserId($scope.activeTab);
				
				$scope.curentIndex = i+1;
				break;
			}
		}
	};

	$scope.onSwipeRight = function(){
		for(var i=0;i< $scope.tabs.length;i++){
			var tempTab = $scope.tabs[i];
			if (tempTab == $scope.activeTab){
				if (i == 0)continue;
				$scope.activeTab = $scope.tabs[i-1];
				$scope.changeClass($scope.activeTab);
				$scope.findActiveUserId($scope.activeTab);
				
				$scope.curentIndex = i-1;
				break;
			}
		}
	};
	
	$scope.setActiveDayTab = function (tab) {     
		　　$scope.activeDayTab = tab; 
	};
	
	$scope.initDayTabs = function(dayTabs){
		$scope.dayTabs = dayTabs;	
		$scope.activeDayTab = dayTabs[1];
	}

	$scope.retrieveDayexamUserInfos = function() {
		
		var url = '/aeaiwm/services/DayWorkExam/rest/retrieve-dayexam-user-infos/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.userInfos = rspJson.userInfos;
			$scope.userCodes = rspJson.userCodes;
			$scope.activeUserCode = $scope.userInfos[0].userCode;
			$scope.activeUserId = $scope.userInfos[0].userId;
			$scope.activeUserId = $scope.userInfos[0].userId;
			$scope.tabs = $scope.userCodes;
			$scope.activeTab = $scope.tabs[0];
			$scope.grpId = rspJson.grpId;
			$scope.findDayWorkInfos();
			
			$scope.totalSize = $scope.userCodes.length;
			$scope.curentIndex = 0;
		});
	}
	$scope.retrieveDayexamUserInfos();

	$scope.findDayWorkInfos = function() {                                       
		var url = '/aeaiwm/services/DayWorkExam/rest/find-daywork-infos/'+$scope.activeUserId+'/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.dayworks = rspJson.dayworks;
			$scope.startTime = rspJson.startTime;
			$scope.endTime = rspJson.endTime;
			$scope.grpName = rspJson.grpName;
			$scope.notes = rspJson.notes;
			
			$scope.setActiveDayTab('currentday');
		});
	}
	$scope.findPastdayWorkListInfos = function() {
		var url = '/aeaiwm/services/DayWorkExam/rest/find-prodayworklist-infos/'+$scope.activeUserId+'/'+$scope.startTime+'/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.dayworks = rspJson.dayworks;
			$scope.startTime = rspJson.startTime;
			$scope.endTime = rspJson.endTime;
			$scope.grpName = rspJson.grpName;
			
			$scope.notes = rspJson.notes;
		});
	}
	
	$scope.findFollowDayWorkListInfos = function() {
		var url = '/aeaiwm/services/DayWorkExam/rest/find-lastdayworklist-infos/'+$scope.activeUserId+'/'+$scope.startTime+'/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.dayworks = rspJson.dayworks;
			$scope.startTime = rspJson.startTime;
			$scope.endTime = rspJson.endTime;
			$scope.grpName = rspJson.grpName;
			
			$scope.notes = rspJson.notes;
		});
	}

	$scope.retrieveCurSeleDayExamInfos = function() {
		var url = '/aeaiwm/services/DayWorkExam/rest/retrieve-cursele-dayinfos/'+$scope.activeUserId+'/'+$scope.startTime+'/'+$scope.grpId;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.dayworks = rspJson.dayworks;
			$scope.startTime = rspJson.startTime;
			$scope.endTime = rspJson.endTime;
			$scope.grpName = rspJson.grpName;
			
			$scope.notes = rspJson.notes;
		});
	}
	
	$scope.changeClass = function(userCode) {
		$scope.activeUserCode = userCode;
	}
	
	$scope.setActiveUserId = function(userId) {
		$scope.activeUserId = userId;
	}
	
	$scope.findActiveUserId = function(userCode) {
		var url = '/aeaiwm/services/DayWorkExam/rest/get-active-userId/'+$scope.activeUserCode;
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.activeUserId = rspJson.userId;
			$scope.retrieveCurSeleDayExamInfos();
		});
	}
	$scope.initTabs = function(tabs){
		$scope.tabs = tabs;	
		$scope.activeTab = tabs[1];
	}	
	
	$scope.createProjectModal = function(){
		AppKit.createModal("${menuCode}","DayProjectInfosModal",$scope);
		//$scope.findProjectInfos();
	}
	
	$scope.findProjectInfos = function(){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						var url = '/aeaiwm/services/DayWorkExam/rest/find-project-infos';
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							$scope.groupInfos = rspJson.projectInfos;
							$scope.grpId = rspJson.projectId;
							$scope.data.grpId = rspJson.projectId;
						});
					}
				})
			}
		});

	}
	$scope.findProjectInfos();
	$scope.changeProject = function(grpId){
		AppKit.isLogin().success(function(data, status, headers, config){
			if (data.result=='true'){
				$scope.userLogin = "isLogin";
				AppKit.secuityOperation("aeaiwm",{"backURL":"/map/repository/genassets/${navCode}/index.cv#/tab/home",
					"success":function(){
						$scope.grpId = grpId;
						var url = '/aeaiwm/services/DayWorkExam/rest/sync-user-projectId/'+grpId;
						var promise = AppKit.getJsonApi(url);
						promise.success(function(rspJson){
							if ("success" == rspJson){
								$scope.retrieveDayexamUserInfos();
							}else{
								AppKit.errorPopup();
							}
						});
					}
				})
			}
		})

	}
	
});